Simplistic Blog Application
===========================

A very short readme file to say that this repository contains the very simple
blog application *developed* during the
[Web Application Architectures](https://www.coursera.org/course/webapplications)
course run from 2014-08-11 to 2014-09-22 at Coursera.org.

Anyway, I finished the course on 2013-09-01... That means viewing all the video
lectures, answering all the quizzes and submitting the four assignments.

